import { useState } from 'react'
import confetti from 'canvas-confetti'
import { TURNS } from './constants.js'
import { checkwinnerFrom, checkEndGame } from './logic/board.js'
import { WinnerModal } from './components/WinnerModal.jsx'
import { resetGameToStorage, saveGameToStorage } from './logic/storage.js'
import { Turn } from './components/Turn.jsx'
import { Board } from './components/Board.jsx'

function App () {
  // estado del tablero
  const [board, setBoard] = useState(() => {
    const boardFromLocalStorage = window.localStorage.getItem('board')

    if (boardFromLocalStorage) return JSON.parse(boardFromLocalStorage)

    return Array(9).fill(null)
  })

  // de quien es el turno
  const [turn, setTurn] = useState(() => {
    const turnFromLocalStorage = window.localStorage.getItem('turn')
    return JSON.parse(turnFromLocalStorage) ?? TURNS.X
  })

  // si es null la partida no termino, si es false la partida termino en empate, sino guarda quien es el ganador
  const [winner, setWinner] = useState(null)

  const updateBoard = (index) => {
    // si en el cuadro donde se hizo clic ya hay algo o la partida termino no es necesario actualizar el tablero
    if (board[index] || winner) return

    // actualizo estado del tablero
    const newBoard = [...board]
    newBoard[index] = turn
    setBoard(newBoard)

    // actualizo de quien es el turno
    const newTurn = turn === TURNS.X ? TURNS.O : TURNS.X
    setTurn(newTurn)

    // guardamos en localstora para poder continuar la partido
    saveGameToStorage({ board: newBoard, turn: newTurn })

    const newWinner = checkwinnerFrom(newBoard)
    if (newWinner) {
      // ya hay un ganador
      confetti()
      setWinner(newWinner)
    } else if (checkEndGame(newBoard)) {
      // el juego termino en empate
      setWinner(false)
    }
  }

  const resetGame = () => {
    setBoard(Array(9).fill(null))
    setTurn(TURNS.X)
    setWinner(null)

    resetGameToStorage()
  }

  return (
    <main className='board'>
      <h1>Tic tac toe</h1>
      <button onClick={resetGame}>Empezar de nuevo</button>

      <Board board={board} updateBoard={updateBoard} />

      <Turn turn={turn} />

      <WinnerModal winner={winner} resetGame={resetGame} />
    </main>
  )
}

export default App
